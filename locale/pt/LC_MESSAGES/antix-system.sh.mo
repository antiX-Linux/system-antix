��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >            4   7     l     |     �     �  j   �  y   )  �   �     S     d  4   u  {   �     &     >      [     |     �  F   �     �     �  d     �   t  -     "   4     W     q       Z   �     �       +        =  V   C  @   �     �     �     �                +     L  D   d     �  '   �  %   �          -     ;  #   V     z           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:23+0300
Last-Translator: José Vieira <jvieira33@sapo.pt>
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Alterar idioma Alterar a ordem do rato Escolher localização para as aplicações do antiX Limpar registos Limpar Cache de Pacotes Domínio do computador Nome do computador Nome de computador alterado de $OLDNAME para $NAME e nome de domínio alterado de $OLDDOMAIN para $DOMAIN  Nome de computador alterado de $OLDNAME para $NAME e Grupo de Trabalho Samba alterado de $OLDWORKGROUP para $NEWWORKGROUP Nome de computador alterado de $OLDNAME para $NAME, nome de domínio alterado de $OLDDOMAIN para $DOMAIN e grupo de trabalho Samba alterado de $OLDWORKGROUP para $NEWWORKGROUP Gestão de disco Gestão_de_Disco Nome de domínio alterado de $OLDDOMAIN para $DOMAIN Nome de domínio alterado de $OLDDOMAIN para $DOMAIN e grupo de Trabalho Samba alterado de $OLDWORKGROUP para $NEWWORKGROUP Ativar Gestão de Disco Ativar a reparação do GRUB Ativar opções de localização Ativar alteração de nome Grub Erro impossível, os botões de rádio requerem pelo menos uma escolha Instalar no MRB Instalar na partição root Teclado definido para $KEYMAP; reiniciar e usar o teclado fazendo ALT + SHIFT + (ESQUERDA / DIREITA) Teclado definido para $KEYMAP; reiniciar e usar o teclado fazendo ALT + SHIFT + (ESQUERDA / DIREITA). Configuração do rato definida para $MOUSE Esquema de teclado deixado como pré-definido O idioma foi alterado para $LOCALE Deixar a pré-definição Mão esquerda Localização Localização escolhida; localização de aplicações do antiX deixada como pré-definida Opções de localização Localizações Configuração do rato definida para $MOUSE Nomes Énecessário adicionar informação válida para alterar as opções de localização É necessário adicionar informação válida para alterar nomes Nenhum selecionado Opções Reparar o GRUB Mão direita Executar antiX para USB Executar o gestor de partições Grupo de trabalho Samba Grupo de trabalho Samba alterado de $OLDWORKGROUP para $NEWWORKGROUP Selecionar a partição root Selecionar disco de arranque do sistema Mostrar utilização básica de disco Atualizar o menu do GRUB Onde instalar ativar seleção de idioma selecionar localização da consola selecionar esquema de teclado 