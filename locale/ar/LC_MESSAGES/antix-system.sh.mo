��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  +  >     j     �  "   �     �     �     �       x   $  �   �  �   -     �       ;   -  �   i      �       "   7  $   Z       R   �     �  0   �  �     �   �  N   �  )     1   ?     q     �  h   �     �       2   -     `  j   o  ]   �     8     S     `     p  E   �      �     �  R        Z  %   t  3   �  )   �  #   �        /   =  -   m           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:21+0300
Last-Translator: Michel Massoud <michel_massoud@hotmail.com>
Language-Team: Arabic (http://www.transifex.com/anticapitalista/antix-development/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Poedit 1.8.11
 تغيير اللغة غير نظام الماوس  اختار لغة برامج antiX محي السجلات محي خابية الحزمة نطاق الحاسوب أسم الحاسوب تغير اسم الحاسوب من $OLDNAME إلى $NAME و تغير اسم النطاق من $OLDDOMAIN إلى $DOMAIN تغير اسم الحاسوب من $OLDNAME إلى $NAME و  تغير اسم مجموعة عمل Samba من $OLDWORKGROUP إلى $NEWWORKGROUP تغير اسم الحاسوب من $OLDNAME إلى $NAME و تغير اسم النطاق من $OLDDOMAIN إلى $DOMAIN و  تغير اسم مجموعة عمل Samba من $OLDWORKGROUP إلى $NEWWORKGROUP إدارة القرص إدارة الأقراص  تغير اسم النطاق من $OLDDOMAIN إلى $DOMAIN تغير اسم النطاق من $OLDDOMAIN إلى $DOMAIN و  تغير اسم مجموعة عمل Samba من $OLDWORKGROUP إلى $NEWWORKGROUP تمكين إدارة القرص تمكين إصلاح grub تمكين خيارات اللغة تمكين تغيير الأسامي Grub  خطأ، أزرار الراديو تتطلب على الاقل خيار واحد ثبتّ على MBR ثبتّ على قسم الجذر root partition تعيين لوحة المفاتيح إلى  $KEYMAP, الرجار إعادة التشغيل و إستعمال حريطة المفاتيح بالضغط على (ALT + SHIFT + (LEFT / RIGHT تعيين لوحة المفاتيح إلى  $KEYMAP, الرجار إعادة التشغيل و إستعمال حريطة المفاتيح بالضغط على (ALT + SHIFT + (LEFT / RIGHT. تعيين تحطيط الماوس إلى $MOUSE خريطة المفاتيح تركت  على الإعداد الافتراضي تم تغيير اللغة إلى $LOCALE حافظ على الإعداد الافتراضي اليد اليسرى  اللغة تم اختيار اللغة, تركت لغة برامج antiX  على الإعداد الافتراضي خيارات اللغة  خيارات اللغة . تعيين تحطيط الماوس إلى $MOUSE الأسامي تحتاج إلى إضافة بعض المعلومات الصحيحة لتغيير خيارات اللغة نحتاج إلى إضافة بعض المعلومات الصحيحة لتغيير أسماء لم يتم التحديد خيارات إصلاح grub اليد اليمنى تنفيذ antiX إلى  الناقل التسلسلي العام USB تنفيذ تقسيم القرص مجموعة عمل Samba  تغير اسم مجموعة عمل Samba من $OLDWORKGROUP إلى $NEWWORKGROUP حدد  قسم الجذر حدد قرص تمهيد النظام عرض الاستخدام الأساسي للقرص تحديث قائمة خريارات grub أين ترغب في التثبيت تمكين تحديد اللغة إخثار لغة المحاورة النصية إخثار لغة لوحية المفاتيح 