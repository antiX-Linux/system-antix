��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >       !   .  7   P     �     �     �     �  s   �  t   B  �   �     h       :   �  x   �      J     k  &   �     �     �  .   �     �       Y   '  �   �  "        *     G     O     W  D   h     �     �  *   �       Z     C   g     �     �     �     �     �  #   �       J   4          �     �     �     �     �          ,           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:22+0300
Last-Translator: secipolla <secipolla@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
 Modificar idioma Modificar configuração do mouse Escolha a regionalização para os aplicativos do antiX Limpar logs Limpar cache de pacotes Nome de domínio Nome da máquina Nome da máquina foi modificado de $OLDNAME para $NAME e nome de domínio foi modificado de $OLDDOMAIN para $DOMAIN Nome da máquina foi modificado de $OLDNAME para $NAME e grupo de trabalho Samba de $OLDWORKGROUP para $NEWWORKGROUP Nome da máquina foi modificado de $OLDNAME para $NAME, nome de domínio foi modificado de $OLDDOMAIN para $DOMAIN e grupo de trabalho Samba de $OLDWORKGROUP para $NEWWORKGROUP Gerenciamento do disco Gerenciamento do disco Nome de domínio foi modificado de $OLDDOMAIN para $DOMAIN Nome de domínio foi modificado de $OLDDOMAIN para $DOMAIN e grupo de trabalho Samba de $OLDWORKGROUP para $NEWWORKGROUP Habilitar gerenciamento do disco Habilitar reparo do GRUB Habilitar opções de regionalização Habilitar mudança de nomes GRUB Erro, os botões requerem ao menos uma escolha Instalar no MBR Instalar na partição raiz Teclado definido para $KEYMAP, reinicie e use o mapa com ALT + SHIFT + (ESQUERDA/DIREITA) Teclado definido para $KEYMAP, reinicie e use o mapa com ALT + SHIFT + (ESQUERDA/DIREITA). Disposição do mouse definida para $MOUSE Mapa do teclado deixado no padrão Idioma alterado para $LOCALE Padrão Canhoto Regionalização Regionalização escolhida, aplicativos do antiX deixados no padrão Opções de regionalização Regionalização Disposição do mouse definida para $MOUSE Nomes É preciso incluir uma informação válida para modificar as opções de regionalização É preciso incluir uma informação válida para modificar os nomes Nenhum selecionado Opções Reparo do GRUB Destro Instalar antiX no pendrive Executar gerenciador de partições Grupo de trabalho Samba Grupo de trabalho Samba foi modificado de $OLDWORKGROUP para $NEWWORKGROUP Selecionar partição raiz Selecionar disco de boot Exibir uso básico do disco Atualizar menu do GRUB Local de instalação Seleção de idioma Regionalização do console Mapa do teclado 