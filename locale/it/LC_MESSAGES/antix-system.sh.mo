��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >          )  '   <     d     s     �     �  j   �  y   $  �   �     F     U  ?   d  |   �     !     8  !   Q     s     �  .   �     �     �  s   �  �   b  '     &   )     P     g     u  C   |     �     �  $   �       \     F   e     �     �     �     �     �     �       N   '     v     �      �     �     �     �          )           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:22+0300
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Cambia lingua Impostazione mouse Scegli lingua per le applicazioni antiX Pulisci i Logs Pulisci la cache dei pacchetti Dominio del computer Nome del computer Il nome del computer è stato modificato da $OLDNAME a $NAME e il nome del dominio da $OLDDOMAIN a $DOMAIN Il nome del computer è stato modificato da $OLDNAME a $NAME e il gruppo di lavoro Samba da $OLDWORKGROUP a $NEWWORKGROUP Il nome del computer è stato modificato da $OLDNAME a $NAME , il nome del dominio da $OLDDOMAIN a $DOMAIN e il gruppo di lavoro Samba da $OLDWORKGROUP a $NEWWORKGROUP Gestione disco Gestione_disco Il nome del dominio è stato modificato da $OLDDOMAIN a $DOMAIN Il nome del dominio è stato modificato da $OLDDOMAIN a $DOMAIN e il gruppo di lavoro Samba da $OLDWORKGROUP a $NEWWORKGROUP Abilita Gestione disco Abilita riparazione grub Abilita opzioni di localizzazione Abilita cambio del nome Grub Errore, almeno una scelta deve essere spuntata Installa su MBR Installa nella partizione root Tastiera impostata su $KEYMAP, riavvia e utilizza la mappatura della tastiera con ALT + SHIFT + (SINISTRA / DESTRA) Tastiera impostata su $KEYMAP, riavvia e utilizza la mappatura della tastiera con ALT + SHIFT + (SINISTRA / DESTRA) Il layout del mouse è impostato su $MOUSE Mappatura tastiera lasciata predefinita La lingua è stata cambiata in $LOCALE Lascia impost. default Mano sinistra Lingua Lingua scelta, lingua delle applicazioni antiX lasciata predefinita Opzioni localizzazione Lingua Layout del Mouse impostato su $MOUSE Nomi E' necessario aggiungere delle informazioni valide per cambiare le opzioni di lingua locale. E' necessario aggiungere delle informazioni valide per cambiare i nomi Nessuno selezionato Opzioni Ripara grub Mano destra Esegui antiX da USB Avvia gestore delle partizioni Gruppo lavoro Samba Il gruppo di lavoro Samba è stato modificato da $OLDWORKGROUP a $NEWWORKGROUP Scegli la partizione root Seleziona il disco del boot Mostra l'utilizzo base del disco Aggiorna il menù di grub Dove installare abilita scelta lingua scegli lingua del terminale scegli mappatura tastiera 