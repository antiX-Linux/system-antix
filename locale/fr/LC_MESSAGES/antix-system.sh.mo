��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >       "   #  3   F     z     �     �     �  x   �  �   N  �   �     |     �  F   �  �   �  !   �     �  #   �     �       ?        K  $   c  m   �  �   �  *   �      �     �     �     �  =   �     1     @     H     _  \   d  Q   �          '     /     =     I  &   a     �  X   �  !   �  1     (   M     v     �  "   �  6   �     �           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:22+0300
Last-Translator: cyril cottet <cyrilusber2001@yahoo.fr>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
 Changer la langue Changement de l'ordre de la souris Choisissez votre locale pour les applications antiX Effacer les journaux Effacer le cache des paquets Domaine informatique Nom de l'ordinateur Le Nom de l'ordinateur a été changé de $OLDNAME pour $NAME et le Nom de domaine à partir de  $OLDDOMAIN pour $DOMAIN Le Nom de l'ordinateur a été changé de $OLDNAME  pour $NAME et le groupe de travail Samba de $OLDWORKGROUP pour $NEWWORKGROUP Le Nom de l'ordinateur a été changé de $OLDNAME pour $NAME le nom de domaine de $OLDDOMAIN pour $DOMAIN et le groupe de travail Samba de $OLDWORKGROUP pour $NEWWORKGROUP Gestionnaire de disque Gestionnaire de disque Le nom de domaine a été changé à partir de $OLDDOMAIN pour $DOMAIN Le nom de domaine a été changé à partir de $OLDDOMAIN pour $DOMAIN et le groupe de travail Samba à partir de $OLDWORKGROUP pour $NEWWORKGROUP Activer le gestionnaire de disque Activer la réparation du grub Activer les options de localisation Activer le changement de nom Grub Erreur impossible, boutons radio nécessitent au moins un choix Installation sur le MBR Installez-le sur la partition racine Clavier fixé à $KEYMAP, redémarrer et utiliser la disposition du clavier par ALT + MAJ + (GAUCHE / DROITE) Clavier fixé à $KEYMAP, redémarrer et utiliser la disposition du clavier par ALT + MAJ + (GAUCHE / DROITE).  Souris réglé sur $MOUSE Disposition du clavier laissé par défaut La langue a changé pour $LOCALE Laisser par défaut Main gauche Locale Locale choisi, application d'antix locale laissé par défaut Options Locale Locales Souris fixé à $MOUSE Noms Il est nécessaire d'ajouter certaines informations valides pour changer les options locales Il est nécessaire d'ajouter certaines informations valides pour changer les noms Aucun sélectionné Options Réparer grub Main droite Exécuter antiX sur USB Exécuter le gestionnaire de partition Groupe de travail Samba Le groupe de travail Samba a été changé à partir de $OLDWORKGROUP pour $NEWWORKGROUP Sélectionnez la partition racine Sélectionnez le disque de démarrage du système Montrer l'utilisation de base du disque  Mettre à jour le menu de grub Où installer Activer la séléction du language Sélectionnez les paramètres régionaux de la console Sélectionnez le clavier 