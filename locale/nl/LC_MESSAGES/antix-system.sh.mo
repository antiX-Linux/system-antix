��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >          +  #   F     j  !   }     �     �  `   �  n     �   �     '     5  4   C  m   x     �     �          -     L  8   Q     �     �  _   �  y        �     �     �     �     �  :        <     J     [     t  K   z  ?   �                     0     =     T     o  A        �     �     �               2     F     `           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:22+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Dutch (http://www.transifex.com/anticapitalista/antix-development/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Veranderen van taal Muis-instelling veranderen Kies uw taal voor antiX-applicaties Logbestanden legen Bewaarplaats voor pakketten legen Computer-domein Computer-naam De computer-naam is omgezet van $OLDNAME naar $NAME en de domeinnaam van $OLDDOMAIN naar $DOMAIN De computer-naam is omgezet van $OLDNAME naar $NAME en de Samba Werkgroep van $OLDWORKGROUP naar $NEWWORKGROUP De computer-naam is omgezet van $OLDNAME naar $NAME, de domeinnaam van $OLDDOMAIN naar $DOMAIN en de Samba Werkgroep van $OLDWORKGROUP naar $NEWWORKGROUP Schijf-beheer Schijf-beheer Domein-naam is veranderd van $OLDDOMAIN naar $DOMAIN Domein-naam is omgezet van $OLDDOMAIN naar $DOMAIN en de Samba Werkgroep van $OLDWORKGROUP naar $NEWWORKGROUP Schijf-beheer instellen Grub-herstel instellen Lokale opties aanzetten Verandering van naam instellen Grub Onmogelijk, er moet minstens een optie aangevinkt worden Op MBR installeren Op de root-partitie installeren Toestenbord ingesteld op $KEYMAP, herstarten en indeling gebruiken met ALS+SHIFT+(LINKS/RECHTS) Toetsenbord ingesteld op $KEYMAP, herstarten en indeling gebruiken met ALT+SHIFT+(LINKS/RECHTS). Muis ingesteld op $MOUSE Toetsenbord-standaard behouden Taal is ingesteld op $LOCALE Standaard behouden Linkshandig Taalinstelling Taal gekozen. Taal voor antiX-applicaties blijft standaard Lokale opties Taalinstellingen Muis ingesteld op $MOUSE Namen Er is enige bruikbare informatie nodig om de taalinstellingen te veranderen Er is enige bruikbare informatie nodig om de naam te veranderen Niets geselecteerd Opties Grub herstellen Rechtshandig Antix to USB uitvoeren Partitie-manager gebruiken Samba Werkgroep Samba Werkgroep is veranderd van $OLDWORKGROUP naar $NEWWORKGROUP Kies root-partitie Kies systeem opstart-schijf Basaal schijf-gebruik tonen Grub-menu updaten Waar te installeren Taalkeuze instellen Kies taal voor de console Kies toetsenbord-indeling 