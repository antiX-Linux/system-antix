��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >     2     @     W     w     �     �     �  b   �  u   3  �   �     P     ^  /   l  t   �          '     ;     T     j  I   o     �  !   �  q   �  �   a  (   �     #     A     T     `  ;   f     �     �  %   �     �  <   �  <   $     a  	   p     z     �     �     �     �  B   �       '   7  #   _     �     �     �     �     �           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:21+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.11
 Změnit jazyk Změnit pořadí myši Zvolit jazyk pro aplikace antiX Vyčistit záznamy Vyčistit mezipamět balíčku Doména počítače Název počítače Název počítače změněn z $OLDNAME na  $NAME a název domény změněn z $OLDDOMAIN na $DOMAIN Název počítače změněn z $OLDNAME na  $NAME a pracovní skupina Samba změněna z $OLDWORKGROUP na $NEWWORKGROUP Název počítače změněn z $OLDNAME na  $NAME, Název domény změněn z $OLDDOMAIN na $DOMAIN a pracovní skupina Samba změněna z $OLDWORKGROUP na $NEWWORKGROUP Správa disku Správa_Disku Název domény změněn z $OLDDOMAIN na $DOMAIN Název domény změněn z $OLDDOMAIN na $DOMAIN a pracovní skupina Samba změněna z $OLDWORKGROUP na $NEWWORKGROUP Povolit správu disku Povolit opravu grub Povolit možnosti jazyka Povolit změnu názvu Grub Nemožná chyba, přepínací tlačítka potřebují alespoň jednu volbu Nainstalovat na MBR Nainstalovat na kořenový oddíl Klávesnice nastavena na $KEYMAP, restartujte a mapu klávesnice požijte pomocí ALT + SHIFT + (DOLEVA/ DOPRAVA) Klávesnice nastavena na $KEYMAP, restartujte a mapu klávesnice požijte pomocí ALT + SHIFT + (DOLEVA/ DOPRAVA). Rozvržení myši nastaveno na $MOUSE Mapa klávesnice ponechána na výchozí Jazyk byl změněn na $LOCALE Nechat standardní Pro leváky Jazyk Jazyk zvolen, jazyk aplikací antix ponechán na výchozím Možnosti jazyka Jazyky Rozvržení myši změněno na $MOUSE Názvy Je třeba přidat jisté platné informace pro změnu jazyka Je třeba přidat jisté platné informace pro změnu názvu Nic nevybráno Možnosti Opravit grub Pro praváky Spustit antiX na USB Spustit správce oddílů Pracovní skupina Samba Pracovní skupina Samba změněna z $OLDWORKGROUP na $NEWWORKGROUP Vybrat kořenový oddíl Vybrat systémový disk pro zavádění Zobrazit základní použití disku Aktualizovat menu grub Kam nainstovat povolit výběr jazyka vybrat jazyk konzole vybrat mapu klávesnice 