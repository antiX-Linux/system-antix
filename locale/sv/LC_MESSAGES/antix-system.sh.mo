��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >  
          $   ,     Q     b     s  	   �  j   �  m   �  �   c          #  5   2  r   h     �     �          /     H  8   M     �     �  g   �  �     *   �  "   �     �            ;        R     d     k     �  G   �  B   �  
        &     6     D     K     ]     v  E   �     �     �  #   �          2     J  "   `     �           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:23+0300
Last-Translator: Henry Oquist <henryoquist@comhem.se>
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Byt språk Byt mus-ordning Välj ditt språk för antiX program Töm loggfilerna Töm Paket-cache Dator-domän Datornamn Datornamn har ändrats från $OLDNAME till $NAME och Domännamn har ändrats från $OLDDOMAIN till $DOMAIN Datornamn har ändrats från $OLDNAME till $NAME och Samba arbetsgrupp från $OLDWORKGROUP till $NEWWORKGROUP Datornamn har ändrats från $OLDNAME till $NAME, Domännamn har ändrats från $OLDDOMAIN till $DOMAIN och Samba arbetsgrupp har ändrats från $OLDWORKGROUP till $NEWWORKGROUP Diskhantering Disk_Hantering Domännamn har ändrats från $OLDDOMAIN till $DOMAIN Domännamn har ändrats från $OLDDOMAIN till $DOMAIN och Samba arbetsgrupp från $OLDWORKGROUP till $NEWWORKGROUP Möjliggör Diskhantering Möjliggör grub-reparation Möjliggör språk-alternativ Möjliggör namnändring Grub Omöjligt fel, radioknappar behöver åtminstone ett val Installera på MBR Installera på root partition Tangentbordet inställt på $KEYMAP, starta om och använd layout med ALT + SHIFT + (VÄNSTER / HÖGER) Tangentbordet inställt på $KEYMAP, starta om och använd layout med ALT + SHIFT + (VÄNSTER / HÖGER). Muslayout inställd på $MOUSE Tangentbords-layouten bibehåller standard Språket har ändrats till $LOCALE Behåll standard Vänster Språk Språk valt, antix -programmens språk fortfarande standard Språk-alternativ Språk Mus-Layout inställd på $MOUSE Namn Behöver lägga till giltig information föt att byta region-alternativ Behövs lägga till någon giltig information för att ändra namn Ingen vald Valmöjligheter Reparera grub Höger Kör antiX to USB Kör partitionshanterare Samba arbetsgrupp Samba arbetsgrupp har ändrats från $OLDWORKGROUP till $NEWWORKGROUP Välj root partition Välj system boot disk Visa grundläggande diskanvändning Uppdatera grub meny Var ska det installeras Möjliggör språkval Välj språk för kommandofönster Välj tangentbordslayout 