��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >          %  7   A     y     �     �     �  h   �  u   <  �   �     L     ]  <   n  y   �     %      C  $   d     �     �  K   �     �       {   '  �   �  *   P  "   {  !   �     �     �  R   �     0     J  /   Y     �  N   �  :   �          5     >     K     X  $   n     �  I   �  !   �  *         A     b     ~  "   �  )   �     �           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:22+0300
Last-Translator: Richard  Holt <richard.holt@gmail.com>
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Cambiar el idioma Cambiar el orden del ratón Escoger su localización para las aplicaciones de antiX Borrar los historiales Borrar el Cache de Paquetes Dominio del sistema Nombre del sistema Se ha cambiado el nombre del sistema de $OLDNAME a $NAME y el nombre del dominio de $OLDDOMAIN a $DOMAIN Se ha cambiado el nombre del sistema de $OLDNAME a $NAME y el grupo de trabajo Samba de $OLDWORKGROUP a $NEWWORKGROUP Se ha cambiado el nombre del sistema de $OLDNAME a $NAME, el dominio de $OLDDOMAIN a $DOMAIN y el grupo de trabajo Samba de $OLDWORKGROUP a $NEWWORKGROUP Manejo del disco Manejo del disco El nombre del dominio se ha cambiado de $OLDDOMAIN a $DOMAIN Se ha cambiado el nombre del dominio de $OLDDOMAIN a $DOMAIN y el grupo de trabajo Samba de $OLDWORKGROUP a $NEWWORKGROUP Habilitar el manejo del disco Habilitar la reparación de Grub Habilitar opciones de localizaciones Habilitar el cambio de nombre Grub Error imposible.  El radio de los botones requiere al menos una escogencia. Instalar en MBR Instalar en la partición root Se ajustó el teclado a $KEYMAP, reinicie y use la distribución nueva oprimiendo ALT + MAYÚSCULAS + (IZQUIERDA / DERECHA) Se ajustó el teclado a $KEYMAP, reinicie y use la distribución nueva oprimiendo ALT + MAYÚSCULAS + (IZQUIERDA / DERECHA). La distribución del ratón se ajustó a $MOUSE La mapa del teclado es la dada por defecto Se ha cambiado el idioma a $LOCALE Mantener las opciones por defecto Mano izquierda Localización Escogió localización, aplicación de antiX se mantenga localización por defecto Opciones de localización Localizaciones Se cambió la distribución del ratón a $MOUSE Nombres Debe agregar información válida para cambiar las opciones de localizaciones. Debe agregar información válida para cambiar los nombres No se seleccionó ninguno Opciones Reparar Grub Mano derecha Ejecutar antix to USB Ejecutar un manejador de particiones Grupo de trabajo Samba Se ha cambiado el grupo de trabajo Samba de $OLDWORKGROUP a $NEWWORKGROUP Seleccionar la partición de root Seleccionar el disco de inicio del sistema Mostrar el uso básico del disco Actualizar el menú de Grub ¿Dónde instalar? habilitar la selección del idioma seleccionar localización para la consola seleccionar una mapa de teclado 