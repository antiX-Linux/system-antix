��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  i  >     �  .   �  ;   �     -  &   M  $   t     �  �   �  �   8  �   �  #   �  #   �  H   �  �   -  4   �  2   �  E   *  .   p     �  `   �       7   6  �   n  �     ;   �  $     (   ;     d  $   v  c   �  4   �  
   4  >   ?  
   ~  �   �  o         �     �     �     �  !   �  M   
  !   X  _   z  .   �  A   	  I   K     �     �  &   �  4   �  A   ,           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:23+0300
Last-Translator: Вячеслав Волошин <vol_vel@mail.ru>
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 1.8.11
 Изменить язык Изменение раскладки мыши Выберите язык для приложений antiX Очистить журналы Очистить кэш пакетов Компьютерный домен: Имя компьютера: Имя компьютера было изменено с $OLDNAME на $NAME, и имя домена - с $OLDDOMAIN на $DOMAIN Имя компьютера было изменено с $OLDNAME на $NAME, и рабочая группа Samba - с $OLDWORKGROUP на $NEWWORKGROUP Имя компьютера было изменено с $OLDNAME на $NAME, имя домена - с $OLDDOMAIN на $DOMAIN, и рабочая группа Samba - с $OLDWORKGROUP на $NEWWORKGROUP Управление дисками Управление дисками Имя домена было изменено с $OLDDOMAIN на $DOMAIN Имя домена было изменено с $OLDDOMAIN на $DOMAIN, и рабочая группа Samba - с $OLDWORKGROUP на $NEWWORKGROUP Включить управление дисками Включить восстановление grub Включить варианты языковых установок Включить изменение имени Загрузчик Grub Ошибка, радио-кнопки требуют хотя бы одной выбранной Установить на MBR Установить на корневой раздел Клавиатура установлена на $KEYMAP, перезагрузите и используйте ее по ALT + SHIFT + (LEFT / RIGHT) Клавиатура установлена на $KEYMAP, перезагрузите и используйте ее по ALT + SHIFT + (LEFT / RIGHT). Раскладка мыши установлена на $MOUSE Раскладка остается по умолчанию Язык изменен на $LOCALE Оставить по умолчанию Для левши Языковые установки: Язык выбран, язык приложений antix остается по умолчанию Варианты языковых установок Языки Раскладка мыши установлена на $MOUSE Имена Нужно добавить достоверную информацию, чтобы изменить языковые параметры Нужно добавить достоверную информацию, чтобы изменить имена Ничего не выбрано Настройки Восстановить grub Для правши Запустить antiX на USB Запустить инструмент работы с разделами... Рабочая группа Samba Рабочая группа Samba была изменена с $OLDWORKGROUP на $NEWWORKGROUP Выберите корневой раздел Выбрать системный загрузочный диск Показать использование основного диска Обновить меню grub Где установить включить выбор языка выбрать локализацию консоли выбрать карту раскладки клавиатуры 