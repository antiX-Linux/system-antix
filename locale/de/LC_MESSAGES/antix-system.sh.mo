��    5      �  G   l      �     �     �  )   �  
   �     �     �       `     m   t  �   �     }     �  7   �  o   �     E     \     o     �     �  ;   �     �     �  O     k   R     �     �     �  	    	     
	  4   	     F	     U	     ]	     x	  ;   ~	  2   �	     �	     �	     
  
   
     
     +
     A
  D   R
     �
     �
     �
     �
     �
     �
          *  �  >          !  6   A     x     �     �     �  i   �  q   2  �   �     =     T  8   k  s   �           9  !   R     t     �  J   �     �  "   �  f     �   �       "     !   B     d     q  Z        �     �             M   %  C   s     �     �     �     �     �     	     $  I   8     �  $   �  "   �     �     �       (   .      W           -      &   ,       )       !   $      1      
      .   5              0      "                       (      /           2                          +   #                            3      '          %                  *            	   4           Change language Change mouse order Choose your locale for antiX applications Clear Logs Clear Package Cache Computer domain Computer name Computer name has been changed from $OLDNAME to $NAME and Domain name from $OLDDOMAIN to $DOMAIN Computer name has been changed from $OLDNAME to $NAME and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Computer name name has been changed from $OLDNAME to $NAME, Domain name from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Disk management Disk_Management Domain name has been changed from $OLDDOMAIN to $DOMAIN Domain name has been changed from $OLDDOMAIN to $DOMAIN and Samba workgroup from $OLDWORKGROUP to $NEWWORKGROUP Enable Disk Management Enable grub repair Enable locale options Enable name change Grub Impossible error, radio buttons require at least one choice Install on MBR Install on root partition Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT) Keyboard set to $KEYMAP, restart and use keymap by ALT + SHIFT + (LEFT / RIGHT). Mouse layout set to $MOUSE Keymap left default Language has changed to $LOCALE Leave default Left hand Locale Locale chosen, antix application locale left default Locale options Locales Mouse Layout set to $MOUSE Names Need to add some valid information to change locale options Need to add some valid information to change names None selected Options Repair grub Right hand Run antiX to USB Run partition manager Samba work group Samba workgroup has been changed from $OLDWORKGROUP to $NEWWORKGROUP Select root partition Select system boot disk Show basic disk usage Update grub menu Where to install enable language select select console locale select keyboard map Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-25 01:31+0300
PO-Revision-Date: 2018-09-24 16:21+0300
Last-Translator: alfredmale <maledora4@googlemail.com>
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Sprache ändern Verändern der Maus-Einstellung Wählen Sie Ihre Sprachumgebung für antiX Anwendungen leeren der Protokolle leeren des Paket-Cache-Speicher Rechnerdomain Rechnername Computer-Name hat sich von $OLDNAME zu $NAME und Domain-Name hat sich von $OLDDOMAIN zu $DOMAIN geändert Computer-Name hat sich von $OLDNAME zu $NAME und Samba-Arbeitsgruppe von $OLDWORKGROUP zu $NEWWORKGROUP geändert Computer-Name hat sich von $OLDNAME zu $NAME, Domain-Name von $OLDDOMAIN zu $DOMAIN und Samba-Arbeitsgruppe von $OLDWORKGROUP zu $NEWWORKGROUP geändert Datenträgerverwaltung Datenträgerverwaltung Domain-Name hat sich von $OLDDOMAIN zu $DOMAIN geändert Domain-Name hat sich von $OLDDOMAIN zu $DOMAIN und Samba-Arbeitsgruppe von $OLDWORKGROUP zu $NEWWORKGROUP geändert Aktiviere Datenträgerverwaltung Aktiviere Grub-Reparatur Gebietsschema Optionen aktivieren Aktiviere Namensänderung Grub Unmöglicher Fehler, Options-Schaltflächen erfordern mindestens eine Wahl Installation in den MBR Installation in die root-Partition Tastatur setzen zu $KEYMAP , Neustart und anwenden dieser Einstellung mit ALT + SHIFT + (LEFT / RIGHT) Tastatur setzen zu $KEYMAP , Neustart und anwenden dieser Einstellung mit ALT + SHIFT + (LEFT / RIGHT). Maus-Layout setzen zu $MOUSE Keymap verwendet Standard Sprache wird verändert zu $LOCALE verlassen der Standardeinstellung Linkshändig Gebietsschema Gebietsschema wählen, antiX-Anwendungen werden diesen voreingestellten Standard verwenden Gebietsschema Optionen Übersetzungen Maus-Layout auf $MOUSE gesetzt Namen Sie müssen gültige Informationen hinzufügen, um locale Optionen zu ändern Sie müssen gültige Informationen hinzufügen, um Namen zu ändern Keine ausgewählt Einstellungen Grub-Reparatur Rechtshändig Starte antiX auf USB Partitionswerkzeug starten Samba Arbeitsgruppe Sambe-Arbeitsgruppe hat sich von $OLDWORKGROUP zu $NEWWORKGROUP geändert Wähle root-Partition Wählen Sie das System-Boot-Laufwerk Zeige Basisdatenträger Verwendung Aktualisiere Grub-Menü Wähle den Installationsort ermögliche Sprachauswahl Wahlen Sie das Gebietsschema der Konsole Wählen Sie die Tastaturbelegung 